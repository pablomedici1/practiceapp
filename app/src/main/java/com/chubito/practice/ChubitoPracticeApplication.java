package com.chubito.practice;

import android.app.Application;

import com.chubito.practice.di.component.ApplicationComponent;
import com.chubito.practice.di.component.DaggerApplicationComponent;
import com.chubito.practice.di.module.ApplicationModule;
//import com.squareup.leakcanary.LeakCanary;

public class ChubitoPracticeApplication extends Application {

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        //Uncomment for debugging
        /*if (LeakCanary.isInAnalyzerProcess(this)) {
            // This process is dedicated to LeakCanary for heap analysis.
            // You should not init your app in this process.
            return;
        }
        LeakCanary.install(this);*/

        initializeApplicationComponent();
    }

    private void initializeApplicationComponent() {
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    public ApplicationComponent getApplicationComponent() {
        return mApplicationComponent;
    }
}
