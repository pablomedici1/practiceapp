package com.chubito.practice.data.TwitterRepository.impl;

import android.util.Base64;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.chubito.practice.data.TwitterRepository.TwitterAPI;
import com.chubito.practice.data.TwitterRepository.TwitterRepository;
import com.chubito.practice.domain.model.BearerToken;
import com.chubito.practice.domain.model.searchResponse.SearchResponse;
import com.chubito.practice.utils.Constants;

import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class TwitterRepositoryImpl implements TwitterRepository {

    private static final String URL_TWITTER = "https://api.twitter.com/";
    private static final String API_KEY = "dXEbRbbTR9or8PVjSOx3nViOF";
    private static final String CONSUMER_KEY = "gPh6QWoIZgPJRYbRMfOTQ7q7xDZXQZ0FGd2o012cvdZldAGmTw";
    private static final String GRANT_TYPE = "client_credentials";

    private final TwitterAPI mTwitterAPI;

    @Inject
    public TwitterRepositoryImpl() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder()
                .connectTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout(Constants.CONNECTION_TIMEOUT, TimeUnit.SECONDS)
                .addInterceptor(interceptor)
                .build();

        Gson customGsonInstance = new GsonBuilder().create();

        Retrofit twitterApiAdapter = new Retrofit.Builder()
                .baseUrl(URL_TWITTER)
                .addConverterFactory(GsonConverterFactory.create(customGsonInstance))
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(client)
                .build();

        mTwitterAPI = twitterApiAdapter.create(TwitterAPI.class);
    }

    @Override
    public Observable<BearerToken> getBearerToken() {
        String apiString = API_KEY + ":" + CONSUMER_KEY;
        String authorization = "Basic " + Base64.encodeToString(apiString.getBytes(), Base64.NO_WRAP);
        return mTwitterAPI.getBearerToken(authorization, GRANT_TYPE);
    }

    @Override
    public Observable<SearchResponse> getTweets(String token, String query) {
        return mTwitterAPI.getTweets(token, query);
    }
}
