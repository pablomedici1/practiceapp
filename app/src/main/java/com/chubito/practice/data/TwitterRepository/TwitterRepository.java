package com.chubito.practice.data.TwitterRepository;

import com.chubito.practice.domain.model.BearerToken;
import com.chubito.practice.domain.model.searchResponse.SearchResponse;

import rx.Observable;

public interface TwitterRepository {

    Observable<BearerToken> getBearerToken();
    Observable<SearchResponse> getTweets(String token, String query);

}
