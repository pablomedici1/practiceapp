package com.chubito.practice.data.LocalRepository.impl;

import android.content.Context;
import android.content.SharedPreferences;

import com.chubito.practice.data.LocalRepository.LocalRepository;

import javax.inject.Inject;

public class LocalRepositoryImpl implements LocalRepository {

    private static final String APP_PREFERENCES = "APP_PREFERENCES";
    public static final String APP_TOKEN = "APP_TOKEN";

    private SharedPreferences mSharedPreferences;

    @Inject
    public LocalRepositoryImpl(Context context) {
        mSharedPreferences = context.getApplicationContext().
                getSharedPreferences(APP_PREFERENCES,
                        Context.MODE_PRIVATE);
    }

    @Override
    public void saveToken(String token) {
        SharedPreferences.Editor editor = mSharedPreferences.edit();
        editor.putString(APP_TOKEN, token);
        editor.apply();
    }

    @Override
    public String getToken() {
        return mSharedPreferences.getString(APP_TOKEN, null);
    }
}
