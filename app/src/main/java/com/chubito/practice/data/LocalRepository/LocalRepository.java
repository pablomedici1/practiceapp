package com.chubito.practice.data.LocalRepository;

public interface LocalRepository {

    void saveToken(String token);
    String getToken();

}
