package com.chubito.practice.data.TwitterRepository;

import com.chubito.practice.domain.model.BearerToken;
import com.chubito.practice.domain.model.searchResponse.SearchResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface TwitterAPI {

    @POST("oauth2/token")
    @Headers({ "Content-Type: application/x-www-form-urlencoded;charset=UTF-8"})
    @FormUrlEncoded
    Observable<BearerToken> getBearerToken(@Header("Authorization") String authorization,
                                           @Field("grant_type") String grantType);

    @GET("1.1/search/tweets.json")
    Observable<SearchResponse> getTweets(@Header("Authorization") String authorization,
                                         @Query("q") String query);

}
