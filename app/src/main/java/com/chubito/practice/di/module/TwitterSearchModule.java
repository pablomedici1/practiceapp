package com.chubito.practice.di.module;

import com.chubito.practice.data.LocalRepository.LocalRepository;
import com.chubito.practice.data.TwitterRepository.TwitterRepository;
import com.chubito.practice.di.scope.PerFragment;
import com.chubito.practice.domain.TwitterSearchUseCase;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;

@Module
public class TwitterSearchModule {

    public TwitterSearchModule() {
    }

    @Provides
    @PerFragment
    TwitterSearchUseCase provideTwitterSearchUsecase(LocalRepository localRepository,
                                                     TwitterRepository twitterRepository,
                                                     @Named("ui_thread") Scheduler uiThread,
                                                     @Named("executor_thread") Scheduler executorThread) {

        return new TwitterSearchUseCase(localRepository, twitterRepository, uiThread, executorThread);
    }
}
