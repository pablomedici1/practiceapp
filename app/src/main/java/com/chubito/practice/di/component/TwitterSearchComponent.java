package com.chubito.practice.di.component;

import com.chubito.practice.di.module.TwitterSearchModule;
import com.chubito.practice.di.scope.PerFragment;
import com.chubito.practice.ui.twitterSearch.TwitterSearchFragment;

import dagger.Component;

@PerFragment
@Component(dependencies = ApplicationComponent.class, modules = {TwitterSearchModule.class})
public interface TwitterSearchComponent {

    void inject(TwitterSearchFragment fragment);

}
