package com.chubito.practice.di.component;

import com.chubito.practice.data.LocalRepository.LocalRepository;
import com.chubito.practice.data.TwitterRepository.TwitterRepository;
import com.chubito.practice.di.module.ApplicationModule;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Component;
import rx.Scheduler;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    TwitterRepository provideTwitterSearchRepository();
    LocalRepository provideLocalRepository();

    @Named("ui_thread") Scheduler provideUiThread();
    @Named("executor_thread") Scheduler provideExecutorThread();

}
