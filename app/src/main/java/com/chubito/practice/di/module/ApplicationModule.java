package com.chubito.practice.di.module;

import android.content.Context;

import com.chubito.practice.ChubitoPracticeApplication;
import com.chubito.practice.data.LocalRepository.LocalRepository;
import com.chubito.practice.data.LocalRepository.impl.LocalRepositoryImpl;
import com.chubito.practice.data.TwitterRepository.TwitterRepository;
import com.chubito.practice.data.TwitterRepository.impl.TwitterRepositoryImpl;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

@Module
public class ApplicationModule {

    private final ChubitoPracticeApplication mApplication;

    public ApplicationModule(ChubitoPracticeApplication application) {
        mApplication = application;
    }

    @Provides
    @Singleton
    Context provideApplicationContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    TwitterRepository provideTwitterSearchRepository(TwitterRepositoryImpl twitterSearchRepository) {
        return twitterSearchRepository;
    }

    @Provides
    @Singleton
    LocalRepository provideLocalRepository(LocalRepositoryImpl localRepository) {
        return localRepository;
    }

    @Provides
    @Named("executor_thread")
    Scheduler provideExecutorThread() {
        return Schedulers.newThread();
    }

    @Provides
    @Named("ui_thread")
    Scheduler provideUiThread() {
        return AndroidSchedulers.mainThread();
    }

}
