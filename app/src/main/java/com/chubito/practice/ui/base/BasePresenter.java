package com.chubito.practice.ui.base;

public interface BasePresenter {

    void attachView(BaseView view);

    void onStart();

    void onStop();

    void onPause();

    void onCreate();

}
