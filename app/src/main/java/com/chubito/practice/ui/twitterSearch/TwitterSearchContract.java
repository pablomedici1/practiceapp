package com.chubito.practice.ui.twitterSearch;

import com.chubito.practice.ui.base.BasePresenter;
import com.chubito.practice.ui.base.BaseView;
import com.chubito.practice.ui.model.Tweet;

import java.util.ArrayList;

public interface TwitterSearchContract {

    interface View extends BaseView {
        void showEmptyIndicator();
        void hideEmptyIndicator();
        void showLoadingIndicator();
        void hideLoadingIndicator();
        void hideTweetList();
        void displayTweetList(ArrayList<Tweet> tweets);
        String getQuerySearch();
        void showError();
        void showValidationLengthError();
    }

    interface Presenter extends BasePresenter {
        void loadTweets(String querySearch);
        void onSearchPressed();
    }
}
