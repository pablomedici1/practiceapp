package com.chubito.practice.ui.twitterSearch.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.chubito.practice.R;
import com.chubito.practice.ui.model.Tweet;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TweetsAdapter extends RecyclerView.Adapter<TweetsAdapter.ViewHolder> {

    private Context mContext;
    private ArrayList<Tweet> mTweets;

    public TweetsAdapter(Context context, ArrayList<Tweet> tweets) {
        mContext = context;
        mTweets = tweets;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.iv_user) ImageView userImg;
        @BindView(R.id.tv_tweet_text) TextView tweetText;
        @BindView(R.id.iv_tweet_image) ImageView tweetImg;

        public ViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }
    }

    @Override
    public TweetsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                           int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_tweet,
                parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        Tweet tweet = mTweets.get(position);
        holder.tweetText.setText(tweet.getTweetText());
        Picasso.with(mContext).load(tweet.getUserImg()).into(holder.userImg);
        if (tweet.getUserImg() != null) {
            Picasso.with(mContext).load(tweet.getTweetImg()).into(holder.tweetImg);
        }
    }

    @Override
    public int getItemCount() {
        return mTweets.size();
    }

    /***
     * Replacing list data with new tweets
     * @param tweets {@link ArrayList<Tweet>}
     */
    public void replaceData(ArrayList<Tweet> tweets) {
        mTweets = tweets;
        notifyDataSetChanged();
    }
}
