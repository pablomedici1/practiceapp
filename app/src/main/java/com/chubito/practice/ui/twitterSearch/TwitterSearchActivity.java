package com.chubito.practice.ui.twitterSearch;

import android.content.Intent;
import android.os.Bundle;

import com.chubito.practice.ChubitoPracticeApplication;
import com.chubito.practice.R;
import com.chubito.practice.di.component.DaggerTwitterSearchComponent;
import com.chubito.practice.di.module.TwitterSearchModule;
import com.chubito.practice.ui.base.BaseActivity;

public class TwitterSearchActivity extends BaseActivity {

    @Override
    protected int getContentView() {
        return R.layout.activity_twitter_search;
    }

    @Override
    protected void resolveDaggerDependency() {

        TwitterSearchFragment fragment = ((TwitterSearchFragment) getSupportFragmentManager()
                .findFragmentById(R.id.twitter_fragment));

        ChubitoPracticeApplication chubitoPracticeApplication = (ChubitoPracticeApplication) getApplication();
        DaggerTwitterSearchComponent.builder()
                .applicationComponent(chubitoPracticeApplication.getApplicationComponent())
                .twitterSearchModule(new TwitterSearchModule())
                .build().inject(fragment);
    }

    @Override
    protected void onViewReady(Bundle savedInstanceState, Intent intent) {
        super.onViewReady(savedInstanceState, intent);
        setTitle(getString(R.string.app_full_name));
    }
}
