package com.chubito.practice.ui.twitterSearch;

import com.chubito.practice.domain.TwitterSearchUseCase;
import com.chubito.practice.domain.model.searchResponse.SearchResponse;
import com.chubito.practice.domain.model.searchResponse.Status;
import com.chubito.practice.ui.base.BaseView;
import com.chubito.practice.ui.model.Tweet;
import com.chubito.practice.ui.model.TweetsResponse;
import com.chubito.practice.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import rx.Observer;
import rx.Subscription;
import rx.functions.Func1;

public class TwitterSearchPresenter implements TwitterSearchContract.Presenter {

    private TwitterSearchContract.View mView;

    private TwitterSearchUseCase mTwitterSearchUseCase;

    private Subscription mTwitterSearchSubscription;

    @Inject
    public TwitterSearchPresenter(TwitterSearchUseCase twitterSearchUseCase) {
        mTwitterSearchUseCase = twitterSearchUseCase;
    }

    @Override
    public void loadTweets(String querySearch) {
        mView.hideEmptyIndicator();
        mView.showLoadingIndicator();

        mTwitterSearchSubscription = mTwitterSearchUseCase.execute(querySearch)
                //Transforming to UI Model
                .map(new Func1<SearchResponse, TweetsResponse>() {
                    @Override
                    public TweetsResponse call(SearchResponse searchResponse) {
                        TweetsResponse tweetsResponse = new TweetsResponse();
                        tweetsResponse.setTweets(new ArrayList<Tweet>());
                        tweetsResponse.setCount(searchResponse.getSearchMetadata().getCount());
                        tweetsResponse.setQuery(searchResponse.getSearchMetadata().getCount());

                        for (Status status : searchResponse.getStatuses()) {
                            String tweetImg = (status.getEntities().getMedia() != null) ?
                                    status.getEntities().getMedia().get(0).getMediaUrl() :
                                    null;
                            tweetsResponse.getTweets().add(new Tweet(
                                    status.getText(), status.getUser().getName(),
                                    tweetImg,
                                    status.getUser().getProfileImageUrl()
                            ));
                        }
                        return tweetsResponse;
                    }
                })
                .subscribe(new Observer<TweetsResponse>() {

                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        tweetsFailure();
                }

                    @Override
                    public void onNext(TweetsResponse tweetsResponse) {
                        tweetsReceived(tweetsResponse);
                    }
                });
    }

    private void tweetsReceived(TweetsResponse tweetsResponse) {
        mView.hideLoadingIndicator();
        if (!tweetsResponse.getTweets().isEmpty()) {
            mView.hideEmptyIndicator();
            mView.displayTweetList(tweetsResponse.getTweets());
        } else {
            mView.hideTweetList();
            mView.showEmptyIndicator();
        }
    }

    private void tweetsFailure() {
        mView.hideLoadingIndicator();
        mView.showError();
    }

    @Override
    public void onSearchPressed() {
        String query = mView.getQuerySearch();
        if (query.length() < Constants.MIN_SEARCH_LENGTH) {
            mView.showValidationLengthError();
        } else {
            loadTweets(query);
        }
    }

    @Override
    public void attachView(BaseView view) {
        mView = (TwitterSearchContract.View) view;
    }

    @Override
    public void onStart() {}

    @Override
    public void onStop() {}

    @Override
    public void onPause() {
        if (mTwitterSearchSubscription != null) {
            mTwitterSearchSubscription.unsubscribe();
        }
    }

    @Override
    public void onCreate() {}
}
