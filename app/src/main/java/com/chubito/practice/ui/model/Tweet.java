package com.chubito.practice.ui.model;

public class Tweet {

    private String mTweetText;
    private String mUserName;
    private String mTweetImg;
    private String mUserImg;

    public Tweet(String tweetText, String userName, String twwetImg, String userImg) {
        mTweetText = tweetText;
        mUserName = userName;
        mTweetImg = twwetImg;
        mUserImg = userImg;
    }

    public String getTweetText() {
        return mTweetText;
    }

    public void setTweetText(String tweetText) {
        mTweetText = tweetText;
    }

    public String getUserName() {
        return mUserName;
    }

    public void setUserName(String userName) {
        mUserName = userName;
    }

    public String getTweetImg() {
        return mTweetImg;
    }

    public void setTweetImg(String twwetImg) {
        mTweetImg = twwetImg;
    }

    public String getUserImg() {
        return mUserImg;
    }

    public void setUserImg(String userImg) {
        mUserImg = userImg;
    }
}
