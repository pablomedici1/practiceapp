package com.chubito.practice.ui.twitterSearch;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.chubito.practice.R;
import com.chubito.practice.ui.model.Tweet;
import com.chubito.practice.ui.twitterSearch.adapters.TweetsAdapter;
import com.chubito.practice.utils.Constants;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class TwitterSearchFragment extends Fragment implements TwitterSearchContract.View {

    @Inject
    TwitterSearchPresenter mPresenter;

    @BindView(R.id.rv_search_result_list) RecyclerView mSearchResultList;
    @BindView(R.id.pb_loading_indicator) ProgressBar mLoadingIndicator;
    @BindView(R.id.iv_empty_indicator) ImageView mEmptyIndicator;
    @BindView(R.id.tv_empty_indicator) TextView mTextEmptyIndicator;
    @BindView(R.id.et_search_query) EditText mSearchQuery;

    private Context mContext;
    private TweetsAdapter mTweetsAdapter;
    private View mViewRoot;

    public static TwitterSearchFragment newInstance() {

        Bundle args = new Bundle();

        TwitterSearchFragment fragment = new TwitterSearchFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_twitter_search, container, false);
        mViewRoot = root;
        ButterKnife.bind(this, root);
        initializeList();
        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        mPresenter.attachView(this);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    @Override
    public void onPause() {
        super.onPause();
        mPresenter.onPause();
    }

    private void initializeList() {
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        mSearchResultList.setLayoutManager(layoutManager);

        mTweetsAdapter = new TweetsAdapter(mContext, new ArrayList<Tweet>(0));
        mSearchResultList.setAdapter(mTweetsAdapter);
    }

    @OnClick(R.id.ib_submit_search)
    public void searchPressed() {
        mPresenter.onSearchPressed();
    }

    @Override
    public void showEmptyIndicator() {
        mEmptyIndicator.setVisibility(View.VISIBLE);
        mTextEmptyIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideEmptyIndicator() {
        mEmptyIndicator.setVisibility(View.GONE);
        mTextEmptyIndicator.setVisibility(View.GONE);
    }

    @Override
    public void showLoadingIndicator() {
        mLoadingIndicator.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideLoadingIndicator() {
        mLoadingIndicator.setVisibility(View.GONE);
    }

    @Override
    public void hideTweetList() {
        mSearchResultList.setVisibility(View.GONE);
    }

    @Override
    public void displayTweetList(ArrayList<Tweet> tweets) {
        mSearchResultList.setVisibility(View.VISIBLE);
        mTweetsAdapter.replaceData(tweets);
    }

    @Override
    public String getQuerySearch() {
        return mSearchQuery.getText().toString();
    }

    @Override
    public void showError() {
        Snackbar snackbar = Snackbar
                .make(mViewRoot, getString(R.string.twitter_api_error), Snackbar.LENGTH_LONG)
                .setAction(getString(R.string.twitter_error_retry), new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        mPresenter.onSearchPressed();
                    }
                });

        // Changing message text color
        snackbar.setActionTextColor(Color.WHITE);

        // Changing action button text color
        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);

        snackbar.show();
    }

    @Override
    public void showValidationLengthError() {
        Toast.makeText(mContext, getString(R.string.search_length_error, Constants.MIN_SEARCH_LENGTH),
                Toast.LENGTH_LONG).show();
    }
}
