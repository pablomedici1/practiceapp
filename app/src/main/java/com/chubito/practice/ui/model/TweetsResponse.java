package com.chubito.practice.ui.model;

import com.chubito.practice.ui.model.Tweet;

import java.util.ArrayList;

public class TweetsResponse {

    private ArrayList<Tweet> mTweets;
    private Integer mQuery;
    private Integer mCount;

    public ArrayList<Tweet> getTweets() {
        return mTweets;
    }

    public void setTweets(ArrayList<Tweet> tweets) {
        mTweets = tweets;
    }

    public Integer getQuery() {
        return mQuery;
    }

    public void setQuery(Integer query) {
        mQuery = query;
    }

    public Integer getCount() {
        return mCount;
    }

    public void setCount(Integer count) {
        mCount = count;
    }
}
