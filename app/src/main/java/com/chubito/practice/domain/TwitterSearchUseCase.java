package com.chubito.practice.domain;

import com.chubito.practice.data.LocalRepository.LocalRepository;
import com.chubito.practice.data.TwitterRepository.TwitterRepository;
import com.chubito.practice.domain.model.BearerToken;
import com.chubito.practice.domain.model.searchResponse.SearchResponse;

import javax.inject.Inject;
import javax.inject.Named;

import rx.Observable;
import rx.Scheduler;
import rx.functions.Func1;

public class TwitterSearchUseCase {

    private final LocalRepository mLocalRepository;
    private final TwitterRepository mTwitterRepository;

    private final Scheduler mUiThread;
    private final Scheduler mExecutorThread;

    @Inject
    public TwitterSearchUseCase(LocalRepository localRepository,
                                TwitterRepository twitterRepository,
                                @Named("ui_thread") Scheduler uiThread,
                                @Named("executor_thread") Scheduler executorThread) {
        mLocalRepository = localRepository;
        mTwitterRepository = twitterRepository;
        mUiThread = uiThread;
        mExecutorThread = executorThread;
    }

    public Observable<SearchResponse> execute(String query) {
        String token = mLocalRepository.getToken();
        if (token != null) {
            return executeWithToken(token, query);
        } else {
            return executeWithoutToken(query);
        }
    }

    private Observable<SearchResponse> executeWithToken(String token, String query) {
        return mTwitterRepository.getTweets(token, query)
                .observeOn(mUiThread)
                .onErrorResumeNext(executeWithoutToken(query))
                .subscribeOn(mExecutorThread);
    }

    private Observable<SearchResponse> executeWithoutToken(final String query) {
        return mTwitterRepository.getBearerToken()
                .observeOn(mUiThread)
                .flatMap(new Func1<BearerToken, Observable<SearchResponse>>() {
                    @Override
                    public Observable<SearchResponse> call(BearerToken bearerToken) {
                        String token = "Bearer " + bearerToken.getAccessToken();
                        mLocalRepository.saveToken(token);
                        return mTwitterRepository.getTweets(token, query)
                                .subscribeOn(mExecutorThread)
                                .observeOn(mUiThread);
                    }})
                .subscribeOn(mExecutorThread);
    }

}
