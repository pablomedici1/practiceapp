package com.chubito.practice.utils;

public class Constants {

    /** CONNECTION **/
    public static final int CONNECTION_TIMEOUT = 20;

    /** SEARCH **/
    public static final int MIN_SEARCH_LENGTH = 3;

    public static final int SPLASH_SCREEN_DELAY = 2000;
}
